// server.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors'); // Import cors middleware

const app = express();
const PORT = process.env.PORT || 3001;

// In-memory store for form data
let formDataStore = [];

// Middleware
app.use(cors()); // Use cors middleware before routes
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// POST route to handle form submission
app.post('/api/submit-form', (req, res) => {
  const formData = req.body;
  formDataStore.push(formData); // Store form data in memory
  console.log('Form data stored:', formData);
  res.send('Form data received and stored successfully!');
});

// GET route to fetch stored form data
app.get('/api/get-form', (req, res) => {
  res.json(formDataStore); // Return stored form data
});

// Start server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
