import './App.css';
import Form from './components/Form';


function App() {
  // const router = createBrowserRouter([
  //   {
  //     path: '/',
  //     element: <Form />,
  //   },
  //   {
  //     path: '/form-details',
  //     element: <FormDetails />,
  //   },
  // ]);
    
  return (
    <div className="App">
       {/* <RouterProvider router={router} ></RouterProvider> */}
        <Form />
    </div>
  );
}

export default App;
