// src/Form.js
import React, { useState } from 'react';
import axios from 'axios';

const Form = () => {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    message: ''
  });

  const [formDetails, setFormDetails] = useState([]);
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:3001/api/submit-form', formData);
      console.log(response.data);
    } catch (error) {
      console.error('Error submitting form:', error);
    }
  };
  const handleFetch = async () => {
    try {
      const response = await axios.get('http://localhost:3001/api/get-form');
      console.log(response.data);
        setFormDetails(response.data);
    } catch (error) {
      console.error('Error retrieving form:', error);
    }
  }

  return (
    <div>
      <h2>Contact Form</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={formData.name}
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={formData.email}
            onChange={handleChange}
          />
        </div>
        <div>
          <label htmlFor="message">Message:</label>
          <textarea
            id="message"
            name="message"
            value={formData.message}
            onChange={handleChange}
          ></textarea>
        </div>
        <button type="submit">Submit</button>
      </form>
      <div>
        <button
        onClick={ () => {
            handleFetch();
        }
        }
        >Click here to retrieve info from server!</button>
        {/* display submitted data */}
        <ul>
          {formDetails.map((form, index) => (
            <li key={index}>
              <div>Name: {form.name}</div>
              <div>Email: {form.email}</div>
              <div>Message: {form.message}</div>
            </li>
          ))}
        </ul>
        
      </div>
    </div>
  );
};

export default Form;
