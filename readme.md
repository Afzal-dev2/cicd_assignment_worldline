### CICD Worldline Assignment by WDGET2024074

## Overview

This repo conists of a sample Front-end form written using ReactJS accompanied by NodeJS backend which serves API endpoint for GET and POST requests.

## Features

1. Form Submission using React Front-end and data sent to server via POST request
2. Displaying Submitted Form data in Front-end via GET request

## To run

```
git clone https://gitlab.com/Afzal-dev2/cicd_assignment_worldline/

<!-- To run front-end -->
cd Worldline-form
npm i
npm start

<!-- To run server -->
cd ../Worldline_form_backend
npm i
node index
```

## Test API endpoint

1. For POST request

```
curl -X POST -H "Content-Type: application/json" -d '{"name":"your_username", "email":"your_email","message":"your_message"}' http://localhost:3001/submit-form
```

2. For GET request

```
curl http://localhost:3001/get-form
```

## Hosting Front-end

I've tried to host my front-end react app through gitlab pages but the issue is that the pipeline builds the react app successfully, but while deploying I get this weird error like below:

```
Uploading artifacts...
public: found 12 matching artifact files and directories 
WARNING: Upload request redirected                  location=https://gitlab.com/api/v4/jobs/4764144936/artifacts?artifact_format=zip&artifact_type=archive new-url=https://gitlab.com
WARNING: Retrying...                                context=artifacts-uploader error=request redirected
Uploading artifacts as "archive" to coordinator... 201 Created  id=4764144936 responseStatus=201 Created token=64_RDMQw
Cleaning up project directory and file based variables
00:00
Job succeeded
```

Seems like many people who use Gitlab also face this issue [here.](https://gitlab.com/pages/hugo/-/issues/92)


